/**
 * OperatorService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package vn.t3h.chapter9.proxy;

public interface OperatorService extends java.rmi.Remote {
    public int subtract(int x, int y) throws java.rmi.RemoteException;
    public int additonal(int x, int y) throws java.rmi.RemoteException;
}
