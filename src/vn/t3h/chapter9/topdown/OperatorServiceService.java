/**
 * OperatorServiceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package vn.t3h.chapter9.topdown;

public interface OperatorServiceService extends javax.xml.rpc.Service {
    public java.lang.String getOperatorServiceAddress();

    public vn.t3h.chapter9.topdown.OperatorService getOperatorService() throws javax.xml.rpc.ServiceException;

    public vn.t3h.chapter9.topdown.OperatorService getOperatorService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
