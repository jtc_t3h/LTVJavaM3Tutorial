<!-- AddressBean  has been made a part of scwcdkit package -->
<jsp:useBean id='address' class="vn.t3h.chapter5.bean.AddressBean" scope='session' />

<html>
<body>
  <table>
    <tr>
      <td>Street</td>
      <td><jsp:getProperty name='address' property='street' /></td>
    </tr>
    <tr>
      <td>City</td>
      <td><jsp:getProperty name='address' property='city' /></td>
    </tr>
    <tr>
      <td>State</td>
      <td><jsp:getProperty name='address' property='state' /></td>
    </tr>
    <tr>
      <td>Zip</td>
      <td><jsp:getProperty name='address' property='zip' /></td>
    </tr>
  </table>
</body>
</html>
