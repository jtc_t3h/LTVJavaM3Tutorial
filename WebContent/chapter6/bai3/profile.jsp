<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<%@ include file="common/header.jsp" %>
<body>
<div id="header"></div>
<%@ include file="common/navigator.jsp" %>
  <div id="center">
    <div id="center-left">
      <div class="profile">
        <div class="title-section">Thông tin tài khoản</div>
        <div class="content-section">
          <form action="#" method="post">
            <table width="100%" border="0" cellspacing="1" cellpadding="1">
              <tr>
                <td style="width: 120px;">Tên tài khoản</td>
                <td><input name="username" type="text" id="username" size="30" maxlength="50" readonly="readonly" value="<%=session.getAttribute("username") %>"/></td>
              </tr>
              <tr>
                <td>Họ và tên</td>
                <td><input name="name" type="text" id="name" size="30" maxlength="50" value="<%=session.getAttribute("displayName") %>"/></td>
              </tr>
              <tr>
                <td>Số CMND</td>
                <td><input name="id" type="text" id="id" size="30" maxlength="12" value="<%=session.getAttribute("id") %>"/></td>
              </tr>
              <tr>
                <td>Địa chỉ</td>
                <td>
                  <textarea name="address" cols="40" rows="3" id="address">
                    <%=session.getAttribute("address") %>
                  </textarea>
                </td>
              </tr>
              <tr>
                <td>Số điện thoại</td>
                <td><input name="phone" type="text" id="phone" size="30" maxlength="15" value="<%=session.getAttribute("tel") %>"/></td>
              </tr>
              <tr>
                <td>Email</td>
                <td><input name="email" type="text" id="email" size="30" maxlength="50" value="<%=session.getAttribute("email") %>"/></td>
              </tr>
              <tr>
                <td>Quyền truy cập</td>
                <td><select name="role" id="role">
                    <option value="1">Nhân viên bán vé</option>
                    <option value="2">Quản lý phòng vé</option>
                    <option value="3">Quản trị hệ thống</option>
                </select></td>
              </tr>
              <tr>
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr>
                <td><input type="submit" value="Lưu thông tin" /></td>
                <td><input type="reset" value="Xóa" /></td>
              </tr>
            </table>
          </form>
        </div>
      </div>
      <div class="password">
        <div class="title-section">Thay đổi mật khẩu</div>
        <div class="content-section">
          <form action="#" method="post">
            <table width="100%" cellpadding="1" cellspacing="1" border="0">
              <tr>
                <td style="width: 150px;">Mật khẩu hiện tại</td>
                <td><input name="pass_current" type="password" id="pass_current" size="30" maxlength="50" /></td>
              </tr>
              <tr>
                <td>Mật khẩu mới</td>
                <td><input name="pass_new" type="password" id="pass_new" size="30" maxlength="50" /></td>
              </tr>
              <tr>
                <td>Nhập lại mật khẩu mới</td>
                <td><input name="pass_renew" type="password" id="pass_renew" size="30" maxlength="50" /></td>
              </tr>
              <tr>
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr>
                <td colspan="2"><input name="" type="submit" value="Lưu thông tin" /></td>
              </tr>
            </table>
          </form>
        </div>
      </div>
    </div>
    <%@ include file="common/right.jsp" %>
  </div>
  <%@include file="common/footer.jsp" %>
</body>
</html>