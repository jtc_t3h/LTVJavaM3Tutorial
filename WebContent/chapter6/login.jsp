<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<%@ include file="bai1/common/header.jsp" %>
<body>
<div id="header"></div>
  <div id="navigator">
    <span>Đăng nhập</span>
  </div>
  <!-- message error -->
  <%@ include file="bai1/common/message.jsp" %>
  <div id="center">
    <form action="bai1/login" method="post">
      <table width="auto" border="0" cellspacing="5" cellpadding="0">
        <tr>
          <td>Tên đăng nhập</td>
          <td><input name="username" type="text" size="30" maxlength="50" /></td>
        </tr>
        <tr>
          <td>Mật khẩu</td>
          <td><input name="password" type="password" size="30" maxlength="50" /></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td><input name="submit" type="submit" value="Đăng nhập" /></td>
          <td><input name="remember" type="checkbox" checked="checked" />Ghi nhớ mật khẩu</td>
        </tr>
      </table>
    </form>
  </div>
  <%@include file="bai1/common/footer.jsp" %>
</body>
</html>