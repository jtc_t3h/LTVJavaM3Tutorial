<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="taglibs.jsp" %>
<div id="navigator">
  <div class="nav-left">
    <a href="<c:url value='home.jsp'/>" title="Trang chủ">Trang chủ</a> |
    <a href="#" title="Giới thiệu">Giới thiệu</a> | 
    <a href="#" title="Liên hệ">Liên hệ</a>
  </div>
  <div class="nav-right">
    <c:if test="${username != null }">
      <span>Xin chào <b>${username }</b></span> &nbsp;&nbsp; | <a href="<c:url value='logout.html'/>" title="Đăng xuất">Đăng xuất</a>
    </c:if>
  </div>
</div>
