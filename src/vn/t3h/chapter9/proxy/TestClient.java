package vn.t3h.chapter9.proxy;

import java.rmi.RemoteException;

public class TestClient {

	public static void main(String[] args) throws RemoteException {
		OperatorServiceProxy proxy = new OperatorServiceProxy();
		System.out.println(proxy.additonal(5, 6));
	}

}
