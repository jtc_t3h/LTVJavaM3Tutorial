package vn.t3h.chapter4.model;

import java.io.Serializable;

public class ChiTietDonHang implements Serializable {

	private int id;
	private int id_donhang;
	private int id_sanpham;
	private int soluong;

	public int getId() {
		return id;
	}

	public int getId_donhang() {
		return id_donhang;
	}

	public int getId_sanpham() {
		return id_sanpham;
	}

	public int getSoluong() {
		return soluong;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setId_donhang(int id_donhang) {
		this.id_donhang = id_donhang;
	}

	public void setId_sanpham(int id_sanpham) {
		this.id_sanpham = id_sanpham;
	}

	public void setSoluong(int soluong) {
		this.soluong = soluong;
	}

}
