<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="vn.t3h.chapter3.model.SanPham"%>
<%@page import="vn.t3h.chapter4.model.QLCSDL"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id="nguoidung" scope="session" class="vn.t3h.chapter4.model.NguoiDung" />
<jsp:useBean id="cart" scope="session" class="vn.t3h.chapter4.utils.ShoppingCart" />
<div class="header-area">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="user-menu">
          <ul>
            <li>Xin chào quý khách<b>${nguoidung.email}</b></li>
            <li><a href="giohang.jsp"><i class="fa fauser"></i>Giỏ hàng của tôi</a></li>
            <li><a href="thanhtoan.jsp"><i class="fa fauser"></i>Thanh toán</a></li>
            <%
            	if (nguoidung.getEmail() != null) {
            %>
            <li><a href="dangxuat.htm"><i class="fa fa-user"></i> Đăng xuất</a></li>
            <%
            	} else {
            %>
            <li><a href="#dangnhap"><i class="fa fauser"></i> Đăng nhập</a></li>
            <%
            	}
            %>
          </ul>
        </div>
      </div>

      <div class="col-md-4">
        <div class="header-right">
          <ul class="list-unstyled list-inline">
            <li class="dropdown dropdown-small"><a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">currency :</span><span class="value">USD </span><b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">USD</a></li>
                <li><a href="#">INR</a></li>
                <li><a href="#">GBP</a></li>
              </ul></li>

            <li class="dropdown dropdown-small"><a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">language :</span><span class="value">English </span><b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">English</a></li>
                <li><a href="#">French</a></li>
                <li><a href="#">German</a></li>
              </ul></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End header area -->

<div class="site-branding-area">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <div class="logo">
          <h1>
            <a href="./"><img src="img/LOGO_T3H.png"></a>
          </h1>
        </div>
      </div>

      <div class="col-sm-6">
        <%
        	String id_pr = request.getParameter("id_pr");
        	if (id_pr != null) {
        		QLCSDL qlcsdl = new QLCSDL();
        		SanPham sp = qlcsdl.sanPhamTheoID(Integer.parseInt(id_pr));
        		int soluong = 1;
        		if (request.getParameter("soluong") != null) {
        			soluong = Integer.parseInt(request.getParameter("soluong"));
        		}
        		cart.addItem(id_pr, sp.getTensanpham(), sp.getDongiaKM(), soluong, sp.getHinhanh());
        	}
        %>
        <div class="shopping-item">
          <%
          	NumberFormat formatter = new DecimalFormat("#,###,###");
          	String dongia = formatter.format(cart.getSumOfPrice());
          %>
          <a href="giohang.jsp">Giỏ hàng - <span class="cartamunt"><%=dongia%> vnd</span> <i class="fa fa-shoppingcart"></i> <span class="productcount"><%=cart.getNumOfItems()%></span></a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End site branding area -->

<div class="mainmenu-area">
  <div class="container">
    <div class="row">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
        </button>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li class="active"><a href="index.html">Trang chủ</a></li>
          <li><a href="shop.html">Cửa hàng</a></li>
          <li><a href="single-product.html">Sản phẩm</a></li>
          <li><a href="cart.html">Giỏ hàng</a></li>
          <li><a href="checkout.html">Checkout</a></li>
          <li><a href="#">Category</a></li>
          <li><a href="#">Others</a></li>
          <li><a href="#">Contact</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>