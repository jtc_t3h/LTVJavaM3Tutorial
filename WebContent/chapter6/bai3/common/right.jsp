  <%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
  <%@ include file="taglibs.jsp" %>
  <div id="center-right">
    <div>
      <h2><fmt:message key="label.right.system"/></h2>
      <a href="#"><fmt:message key="label.right.system.profile"/></a>
      <a href="#"><fmt:message key="label.right.system.user"/></a>
      <a href="#"><fmt:message key="label.right.system.road"/></a>
      <a href="#"><fmt:message key="label.right.system.bus"/></a>
    </div>
    <div>
      <h2><fmt:message key="label.right.function"/></h2>
      <a href="#"><fmt:message key="label.right.function.scheduler"/></a>
      <a href="#"><fmt:message key="label.right.function.search"/></a>
      <a href="#"><fmt:message key="label.right.function.book"/></a>
      <a href="#"><fmt:message key="label.right.function.chair"/></a>
    </div>
  </div>