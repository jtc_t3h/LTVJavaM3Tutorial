package vn.t3h.chapter6.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

/**
 * Servlet Filter implementation class EncodingChareterFilter
 */
@WebFilter(urlPatterns="/chapter6/bai1/*", initParams = {@WebInitParam(name="encode", value="utf-8")})
public class Bai1_EncodingChareterFilter implements Filter {

	private ServletContext context;
	private FilterConfig config;
	
    /**
     * Default constructor. 
     */
    public Bai1_EncodingChareterFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		context.log("EncodingCharacter Filter Destroy");
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		context.log("EncodingCharacter Filter doFilter");
		
		request.setCharacterEncoding(config.getInitParameter("encode"));
		response.setCharacterEncoding(config.getInitParameter("encode"));
		
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		this.config = fConfig;
		this.context = fConfig.getServletContext();
		
		context.log("EncodingCharacter Filter init");
	}

}
