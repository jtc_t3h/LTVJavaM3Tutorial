<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
    <jsp:useBean id="address" class="vn.t3h.chapter5.Address" scope="session"/>
	Street: <jsp:getProperty name="address" property="street" /><br>
    City: <jsp:getProperty name="address" property="city" /><br>
    State: <jsp:getProperty name="address" property="state" /><br>
    Zip: <jsp:getProperty name="address" property="zip" /><br>
</body>
</html>