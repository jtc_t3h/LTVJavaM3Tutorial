package vn.t3h.chapter9;

import javax.xml.ws.Endpoint;

public class HelloWorldEndpoint {

	public static void main(String[] args) {
		Endpoint.publish("http://localhost:8080/LTVJavaM3Tutorial/services/hello", new HelloWorldImpl());
		System.out.println("Server running...");
	}

}
