package vn.t3h.chapter9.stub;

public class TestClient {

	public static void main(String[] args) {
		OperatorServiceService service = new OperatorServiceService();
		OperatorService stub = service.getOperatorService();
		
		System.out.println(stub.additonal(5, 6));
	}

}
