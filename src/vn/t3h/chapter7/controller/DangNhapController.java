package vn.t3h.chapter7.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import vn.t3h.chapter7.dao.NguoiDungDAO;
import vn.t3h.chapter7.domain.NguoiDung;

/**
 * Servlet implementation class Bai1_DangNhapServlet
 */
@WebServlet("/chapter7/mvc/dangnhap.htm")
public class DangNhapController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private NguoiDungDAO ndDAO = new NguoiDungDAO();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DangNhapController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		try {
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			
			NguoiDung nguoidung = ndDAO.dangNhap(email, password);
			if (nguoidung != null) {
				session.setAttribute("nguoidung", nguoidung);
			}
			
		} catch (Exception ex) {
			Logger.getLogger(DangNhapController.class.getName()).log(Level.SEVERE, null, ex);
		}
		response.sendRedirect("index.html");
	}

}
