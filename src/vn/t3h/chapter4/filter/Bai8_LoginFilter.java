package vn.t3h.chapter4.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class LoginFilter
 */
@WebFilter("/chapter4/bai8/*")
public class Bai8_LoginFilter implements Filter {

	private ServletContext context;
	
    /**
     * Default constructor. 
     */
    public Bai8_LoginFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		context.log("Login Filter Destroy");
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		
		HttpSession session = req.getSession();
		context.log("Request URL: " + req.getRequestURI());
		
		if (session.getAttribute("username") == null && 
				!req.getRequestURI().endsWith("login.jsp") &&
				!req.getRequestURI().endsWith("login") && 
				!req.getRequestURI().endsWith("logout.html") &&
				!req.getRequestURI().endsWith(".css") &&
				!req.getRequestURI().endsWith(".png")){
			res.sendRedirect(context.getContextPath() + "/chapter4/bai8/login.jsp");			
		} else {
			chain.doFilter(request, response);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		context = fConfig.getServletContext();
		context.log("Login Filter Init");
	}

}
