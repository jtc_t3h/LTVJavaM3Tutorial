package vn.t3h.chapter7.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.t3h.chapter7.dao.SanPhamDAO;
import vn.t3h.chapter7.domain.SanPham;

/**
 * Servlet implementation class HomeController
 */
@WebServlet("/chapter7/mvc/index.html")
public class HomeController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private SanPhamDAO spDAO = new SanPhamDAO();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomeController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<SanPham> listSP = spDAO.find(0,10);
		request.setAttribute("listSP", listSP);
		
		String id_pr = request.getParameter("id_pr");
		
		
		request.getRequestDispatcher("index.jsp").forward(request, response);;
	}

}
