<%@page import="java.util.List"%>
<%@page import="vn.t3h.chapter3.model.QLCSDL"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="vn.t3h.chapter3.model.SanPham"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initialscale=1">
<title>Chi tiết sản phẩm</title>
<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
<!-- Bootstrap -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="css/font-awesome.min.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="css/responsive.css">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5
elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via
file:// -->
<!--[if lt IE 9]>
<script
src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script
>
<script
src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
  <!-- main menu-->
  <jsp:include page="mainmenu.jsp"></jsp:include>
  <div class="product-big-title-area">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="product-bit-title text-center">
            <h2>Shopping</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="single-product-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
      <div class="row">
        <!--tim kiem -->
        <jsp:include page="timkiem.jsp"></jsp:include>
        <!--tim kiem -->
        <%
        	SanPham sp = (SanPham) request.getAttribute("sp");
        	if (sp != null) {
        		NumberFormat formatter = new DecimalFormat("#,###,###");
        		String ddHinh = "img/" + sp.getHinhanh();
        		String dongia = formatter.format(sp.getDongia());
        		String dongiaKM = formatter.format(sp.getDongiaKM());
        %>
        <div class="col-md-8">
          <div class="product-content-right">
            <div class="product-breadcroumb">
              <a href="index.jsp">Trang chủ</a> <a href="shop.jsp?id_loai=<%=sp.getId_loai()%>">Tên loại</a> <a href="laysanpham.htm?id_sanpham=<%=sp.getId()%>"> <%=sp.getTensanpham()%>
              </a>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <div class="product-images">
                  <div class="product-main-img">
                    <img src="<%=ddHinh%>" alt="">
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="product-inner">
                  <h2 class="productname"><%=sp.getTensanpham()%></h2>
                  <div class="product-inner-price">
                    <ins><%=dongiaKM%>vnđ
                    </ins>
                    <del><%=dongia%>vnđ
                    </del>
                  </div>
                  <%
                  	int sl = 0;
                  		if (sp.getSoluong() > 0) {
                  			sl = sp.getSoluong();
                  		}
                  %>
                  <form action="laysanpham.htm" class="cart" method="get">
                    <input type="hidden" name="id_sanpham" value="<%=sp.getId()%>"> <input type="hidden" name="id_pr" value="<%=sp.getId()%>">
                    <div class="quantity">
                      <input type="number" class="input-text
qty text" value="1" name="soluong" min="1" max="<%=sl%>" step="1">
                    </div>
                    <button class="add_to_cart_button" type="submit">Giỏ hàng</button>
                  </form>
                  <div role="tabpanel">
                    <ul class="product-tab" role="tablist">
                      <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" datatoggle="tab">Mô tả</a></li>
                      <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" datatoggle="tab">Đánh giá</a></li>
                    </ul>
                    <div class="tab-content">
                      <div role="tabpanel" class="tab-pane fade in active" id="home">
                        <h2>Mô tả</h2>
                        <p><%=sp.getMota()%></p>
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="profile">
                        <h2>Đánh giá</h2>
                        <div class="submit-review">
                          <p>
                            <label for="name">Họ tên</label> <input name="name" type="text">
                          </p>
                          <p>
                            <label for="email">Email</label> <input name="email" type="email">
                          </p>
                          <div class="rating-chooser">
                            <p>Bạn cho điểm</p>
                            <div class="quantity">
                              <input type="number" class="input-text qty text" value="1" name="diem" min="1" max="5" step="1">
                            </div>
                            <div class="rating-wrap-post">
                              <i class="fa fa-star"></i>
                            </div>
                          </div>
                          <p>
                            <label for="review">Ý kiến</label>
                            <textarea name="review" cols="30" rows="10"></textarea>
                          </p>
                          <p>
                            <input type="submit" value="Gửi">
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <%
            	}
            %>
            <div class="related-products-wrapper">
              <h2 class="related-products-title">Sản phẩm liên quan</h2>
              <div class="related-products-carousel">
                <%
                	if (sp != null) {
                		String sql = "Select * from sanpham where id_thuonghieu = " + sp.getId_thuongHieu();
                		QLCSDL qlcsdl = new QLCSDL();
                		List<SanPham> lstSanPhamLienQuan = qlcsdl.dsSanPham(sql);
                		if (lstSanPhamLienQuan != null) {
                			for (SanPham splq : lstSanPhamLienQuan) {
                				NumberFormat formatter = new DecimalFormat("#,###,###");
                				String dd = "img/" + splq.getHinhanh();
                				String dongia = formatter.format(sp.getDongia());
                				String dongiaKM = formatter.format(sp.getDongiaKM());
                %>
                <div class="single-product">
                  <div class="product-f-image">
                    <img src="<%=dd%>" alt="">
                    <div class="product-hover">
                      <a href="" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Giỏ hàng</a> <a href="laysanpham.htm?id_sanpham=<%=splq.getId()%>" class="view-details-link"><i class="fa fa-link"></i> Xem chi tiết</a>
                    </div>
                  </div>
                  <h2>
                    <a href="laysanpham.htm?id_sanpham=<%=splq.getId()%>">< %=splq.getTensanpham()%></a>
                  </h2>
                  <div class="product-carousel-price">
                    <ins><%=dongiaKM%>
                      vnđ
                    </ins>
                    <del><%=dongia%>
                      vnđ
                    </del>
                  </div>
                </div>
                <%
                	}
                		}
                	}
                %>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- footer -->
  <jsp:include page="footer.jsp"></jsp:include>
  <!-- Latest jQuery form server -->
  <script src="https://code.jquery.com/jquery.min.js"></script>
  <!-- Bootstrap JS form CDN -->
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <!-- jQuery sticky menu -->
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.sticky.js"></script>
  <!-- jQuery easing -->
  <script src="js/jquery.easing.1.3.min.js"></script>
  <!-- Main Script -->
  <script src="js/main.js"></script>
</body>
</html>