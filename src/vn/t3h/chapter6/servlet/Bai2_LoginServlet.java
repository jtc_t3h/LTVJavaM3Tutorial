package vn.t3h.chapter6.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sun.reflect.ReflectionFactory.GetReflectionFactoryAction;
import vn.t3h.chapter3.utils.ConfigUtils;

/**
 * Servlet implementation class Bai6_LoginServlet
 */
@WebServlet(urlPatterns="/chapter6/bai2/login", initParams={
		@WebInitParam(name="username", value="admin"), 
		@WebInitParam(name="password", value="welcome")
})
public class Bai2_LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Bai2_LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		String configUsername = getServletConfig().getInitParameter("username");
		String configPassword = getServletConfig().getInitParameter("password");
		
		if (configUsername != null && configPassword != null && configUsername.equalsIgnoreCase(username) && configPassword.equalsIgnoreCase(password)){
			HttpSession session = request.getSession(true);
			session.setAttribute("username", username);
			
			session.setAttribute("displayName", getServletContext().getInitParameter("displayName"));
			session.setAttribute("id", getServletContext().getInitParameter("id"));
			session.setAttribute("address", getServletContext().getInitParameter("address"));
			session.setAttribute("tel", getServletContext().getInitParameter("tel"));
			session.setAttribute("email", getServletContext().getInitParameter("email"));
			
			response.sendRedirect("profile.jsp");
		} else {
			request.setAttribute("msgError", "Tài khoản hoặc mật khẩu không đúng.");
			request.getRequestDispatcher("/chapter6/bai2/login.jsp").forward(request, response);
		}
	}

}
