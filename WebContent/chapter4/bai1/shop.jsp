<%@page import="vn.t3h.chapter3.model.QLCSDL"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="vn.t3h.chapter3.model.SanPham"%>
<%@page import="java.util.List"%>
<%@page import="vn.t3h.chapter3.utils.Pager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initialscale=1">
<title>Sản phẩm</title>
<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
<!-- Bootstrap -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="css/font-awesome.min.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="css/responsive.css">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5
elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via
file:// -->
<!--[if lt IE 9]>
<script
src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script
>
<script
src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
  <jsp:include page="mainmenu.jsp"></jsp:include>
  <div class="product-big-title-area">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="product-bit-title text-center">
            <h2>Shop</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="single-product-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
      <div class="row">
        <%
        	Pager p = new Pager();
        	int limit = 8;
        	int selectPage = 0;
        	if (request.getParameter("page") != null) {
        		selectPage = Integer.parseInt(request.getParameter("page"));
        	}
        	int start = p.findStart(limit, selectPage);
        	QLCSDL qlcsdl = new QLCSDL();
        	String sql = "Select * from sanpham";
        	if (request.getParameter("id_loai") != null) {
        		int id_loai = Integer.parseInt(request.getParameter("id_loai"));
        		sql = "Select * from sanpham where id_loai = '" + id_loai + "'";
        	}
        	if (request.getParameter("id_thuonghieu") != null) {
        		int id_thuonghieu = Integer.parseInt(request.getParameter("id_thuonghieu"));
        		sql = "Select * from sanpham where id_thuonghieu = '" + id_thuonghieu + "'";
        	}
        	int count = qlcsdl.dsSanPham(sql).size();
        	int pages = p.findPages(count, limit);
        	sql = "select * from sanpham limit " + start + ", " + limit + "";
        	if (request.getParameter("id_loai") != null) {
        		int id_loai = Integer.parseInt(request.getParameter("id_loai"));
        		sql = "Select * from sanpham WHERE id_loai = '" + id_loai + "' limit " + start + ", " + limit + "";
        	}
        	if (request.getParameter("id_thuonghieu") != null) {
        		int id_thuonghieu = Integer.parseInt(request.getParameter("id_thuonghieu"));
        		sql = "Select * from sanpham WHERE id_thuonghieu= '" + id_thuonghieu + "' limit " + start + ", " + limit + "";
        	}
        	List<SanPham> listSPPT = qlcsdl.dsSanPham(sql);
        	String ddHinh = "";
        	if (listSPPT.size() > 0) {
        		for (SanPham sp : listSPPT) {
        			ddHinh = "img/" + sp.getHinhanh();
        			NumberFormat formatter = new DecimalFormat("#,###,###");
        			String dongia = formatter.format(sp.getDongia());
        			String dongiaKM = formatter.format(sp.getDongiaKM());
        %>
        <!-- 1 san pham -->
        <div class="col-md-3 col-sm-6">
          <div class="single-shop-product">
            <div class="product-f-image">
              <img src="<%=ddHinh%>" alt="<%=sp.getTensanpham()%>">
            </div>
            <h2>
              <a href="laysanpham.htm?id_sanpham=<%=sp.getId()%>"> <%=sp.getTensanpham()%>
              </a>
            </h2>
            <div class="product-carousel-price">
              <ins><%=dongiaKM%>
                vnđ
              </ins>
              <del><%=dongia%>
                vnđ
              </del>
            </div>
            <div class="product-option-shop">
              <form action="shop.jsp" method="post">
                <input type="hidden" name="id_pr" value="<%=sp.getId()%>"> <input type="submit" value="Giỏ hàng">
              </form>
            </div>
          </div>
        </div>
        <!-- het 1 san pham -->
        <%
        	}
        }
        %>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="product-pagination text-center">
            <nav>
              <ul class="pagination">
                <%
                	if (request.getParameter("id_loai") != null) {
                %>
                   <%=p.pageListQueryString(selectPage, pages, request.getRequestURI(),"&id_loai=" + request.getParameter("id_loai"))%>
                <%
                	} else if (request.getParameter("id_thuonghieu") != null) {
                %>
                <%=p.pageListQueryString(selectPage, pages, request.getRequestURI(),"&id_thuonghieu=" + request.getParameter("id_thuonghieu"))%>
                <%
                	} else {
                %>
                <%=p.pageList(selectPage, pages, request.getRequestURI())%>
                <%
                	}
                %>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>
  <jsp:include page="footer.jsp"></jsp:include>
  <!-- Latest jQuery form server -->
  <script src="https://code.jquery.com/jquery.min.js"></script>
  <!-- Bootstrap JS form CDN -->
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <!-- jQuery sticky menu -->
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.sticky.js"></script>
  <!-- jQuery easing -->
  <script src="js/jquery.easing.1.3.min.js"></script>
  <!-- Main Script -->
  <script src="js/main.js"></script>
</body>
</html>