package vn.t3h.chapter4.model;

import java.io.Serializable;
import java.sql.Date;

public class DonHang implements Serializable {

	private int id;
	private int id_khachhang;
	private Date ngaydathang;
	private String tennguoidathang;
	private String dienthoainguoinhan;
	private String diachigiaohang;
	private String ghichu;
	private int thanhtoan;
	private int id_trangthai;

	public int getId() {
		return id;
	}

	public int getId_khachhang() {
		return id_khachhang;
	}

	public Date getNgaydathang() {
		return ngaydathang;
	}

	public String getTennguoidathang() {
		return tennguoidathang;
	}

	public String getDienthoainguoinhan() {
		return dienthoainguoinhan;
	}

	public String getDiachigiaohang() {
		return diachigiaohang;
	}

	public String getGhichu() {
		return ghichu;
	}

	public int getThanhtoan() {
		return thanhtoan;
	}

	public int getId_trangthai() {
		return id_trangthai;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setId_khachhang(int id_khachhang) {
		this.id_khachhang = id_khachhang;
	}

	public void setNgaydathang(Date ngaydathang) {
		this.ngaydathang = ngaydathang;
	}

	public void setTennguoidathang(String tennguoidathang) {
		this.tennguoidathang = tennguoidathang;
	}

	public void setDienthoainguoinhan(String dienthoainguoinhan) {
		this.dienthoainguoinhan = dienthoainguoinhan;
	}

	public void setDiachigiaohang(String diachigiaohang) {
		this.diachigiaohang = diachigiaohang;
	}

	public void setGhichu(String ghichu) {
		this.ghichu = ghichu;
	}

	public void setThanhtoan(int thanhtoan) {
		this.thanhtoan = thanhtoan;
	}

	public void setId_trangthai(int id_trangthai) {
		this.id_trangthai = id_trangthai;
	}

}
