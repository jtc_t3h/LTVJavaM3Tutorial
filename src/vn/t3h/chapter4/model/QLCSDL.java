package vn.t3h.chapter4.model;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import vn.t3h.chapter3.model.Loai;
import vn.t3h.chapter3.model.SanPham;
import vn.t3h.chapter3.model.ThuongHieu;

public class QLCSDL {

	private Connection connect() throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		String url = "jdbc:mysql://localhost:3306/phuong_perfume?useUnicode=yes&characterEncoding=UTF-8";
		String username = "root";
		String password = "";

		return DriverManager.getConnection(url, username, password);
	}

	public List<SanPham> dsSanPham(String sql) throws SQLException, ClassNotFoundException {
		List<SanPham> rs;

		try (Connection conn = this.connect()) {
			rs = new ArrayList();
			java.sql.Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				SanPham sp = new SanPham();
				sp.setId(resultSet.getInt("id"));
				sp.setTensanpham(resultSet.getString("tensanpham"));
				sp.setMota(resultSet.getString("mota"));
				sp.setHinhanh(resultSet.getString("hinhanh"));
				sp.setDongia(resultSet.getDouble("dongia"));
				sp.setDongiaKM(resultSet.getDouble("dongiaKM"));
				sp.setSoluong(resultSet.getInt("soluong"));
				sp.setNgaytao(resultSet.getDate("ngaytao"));
				sp.setHienthi(resultSet.getInt("hienthi"));
				sp.setId_loai(resultSet.getInt("id_loai"));

				sp.setId_thuongHieu(resultSet.getInt("id_thuonghieu"));
				rs.add(sp);
			}
		}
		return rs;
	}

	public List<ThuongHieu> dsThuongHieu() throws SQLException, ClassNotFoundException {
		List<ThuongHieu> rs;
		try (Connection conn = this.connect()) {
			rs = new ArrayList<>();
			java.sql.Statement statement = conn.createStatement();
			String sql = "SELECT * FROM thuonghieu";
			ResultSet resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				ThuongHieu th = new ThuongHieu();
				th.setId(resultSet.getInt("id"));
				th.setTenthuonghieu(resultSet.getString("tenthuonghieu"));
				th.setHinhanh(resultSet.getString("hinhanh"));
				rs.add(th);
			}
		}
		return rs;
	}

	public List<Loai> dsLoai() throws SQLException, ClassNotFoundException {
		List<Loai> rs;
		try (Connection conn = this.connect()) {
			rs = new ArrayList<>();
			java.sql.Statement statement = conn.createStatement();
			String sql = "SELECT * FROM loai";
			ResultSet resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				Loai loai = new Loai();
				loai.setId(resultSet.getInt("id"));
				loai.setTenloai(resultSet.getString("tenloai"));

				rs.add(loai);
			}
		}
		return rs;
	}

	public NguoiDung dangnhapNguoiDung(String email, String password) throws SQLException, ClassNotFoundException {
		NguoiDung nd = null;
		try (Connection conn = this.connect()) {
			java.sql.Statement statement = conn.createStatement();
			String sql = "SELECT * FROM nguoidung WHERE email like '" + email + "' and password like '" + password
					+ "'";
			ResultSet resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				nd = new NguoiDung();
				nd.setId(resultSet.getInt("id"));
				nd.setEmail(resultSet.getString("email"));
				nd.setPassword(resultSet.getString("password"));
				nd.setHoten(resultSet.getString("hoten"));
				nd.setDiachi(resultSet.getString("diachi"));
				nd.setDtdd(resultSet.getString("dtdd"));
				nd.setId_vaitro(resultSet.getInt("id_vaitro"));
			}
		}
		return nd;
	}

	public SanPham sanPhamTheoID(int id) throws SQLException, ClassNotFoundException {
		SanPham sp = null;

		try (Connection conn = this.connect()) {
			java.sql.Statement statement = conn.createStatement();

			String sql = "SELECT * FROM sanpham WHERE id = " + id;
			ResultSet resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				sp = new SanPham();
				sp.setId(resultSet.getInt("id"));
				sp.setTensanpham(resultSet.getString("tensanpham"));
				sp.setMota(resultSet.getString("mota"));
				sp.setHinhanh(resultSet.getString("hinhanh"));
				sp.setDongia(resultSet.getDouble("dongia"));
				sp.setDongiaKM(resultSet.getDouble("dongiaKM"));
				sp.setSoluong(resultSet.getInt("soluong"));
				sp.setNgaytao(resultSet.getDate("ngaytao"));
				sp.setHienthi(resultSet.getInt("hienthi"));
				sp.setId_loai(resultSet.getInt("id_loai"));

				sp.setId_thuongHieu(resultSet.getInt("id_thuonghieu"));
			}
		}
		return sp;
	}

	public int themDonHang(DonHang donhang) throws SQLException, ClassNotFoundException, UnsupportedEncodingException {
		int lastInsertID = -1;
		try (Connection conn = this.connect()) {
			Statement statement = conn.createStatement();
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
			String sql = "INSERT INTO donhang(id_khachhang,	ngaydathang, tennguoinhanhang, dienthoainguoinhan, diachigiaohang,	ghichu) VALUES('"
					+ donhang.getId_khachhang() + "' ,'" + sf.format(donhang.getNgaydathang()) + "' ,'"
					+ donhang.getTennguoidathang() + "' ,'" + donhang.getDienthoainguoinhan() + "' ,'"
					+ donhang.getDiachigiaohang() + "', '" + donhang.getGhichu() + "')";
			statement.executeUpdate(sql);
			ResultSet rs = statement.executeQuery("select id from donhang order by id desc limit 0,1");
			while (rs.next()) {
				lastInsertID = rs.getInt("id");
				break;
			}
		}
		return lastInsertID;
	}

	// them chi tiet don hang
	public boolean themCTDH(ChiTietDonHang ctdh)
			throws SQLException, ClassNotFoundException, UnsupportedEncodingException {
		boolean execute = false;
		try (Connection conn = this.connect()) {
			String sql = "INSERT INTO chitietdonhang VALUES(NULL,'" + ctdh.getId_donhang() + "' ,'"
					+ ctdh.getId_sanpham() + "' ,'" + ctdh.getSoluong() + "')";
			PreparedStatement statement = conn.prepareStatement(sql);
			execute = statement.execute();
		}
		return execute;
	}
}
