package vn.t3h.chapter7.domain;

import java.io.Serializable;

public class Loai implements Serializable {

	private int id;
	private String tenloai;

	public int getId() {
		return id;
	}

	public String getTenloai() {
		return tenloai;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setTenloai(String tenloai) {
		this.tenloai = tenloai;
	}

}
