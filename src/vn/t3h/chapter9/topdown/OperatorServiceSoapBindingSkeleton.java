/**
 * OperatorServiceSoapBindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package vn.t3h.chapter9.topdown;

public class OperatorServiceSoapBindingSkeleton implements vn.t3h.chapter9.topdown.OperatorService, org.apache.axis.wsdl.Skeleton {
    private vn.t3h.chapter9.topdown.OperatorService impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://chapter9.t3h.vn", "x"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://chapter9.t3h.vn", "y"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("subtract", _params, new javax.xml.namespace.QName("http://chapter9.t3h.vn", "subtractReturn"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://chapter9.t3h.vn", "subtract"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("subtract") == null) {
            _myOperations.put("subtract", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("subtract")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://chapter9.t3h.vn", "x"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://chapter9.t3h.vn", "y"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("additonal", _params, new javax.xml.namespace.QName("http://chapter9.t3h.vn", "additonalReturn"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://chapter9.t3h.vn", "additonal"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("additonal") == null) {
            _myOperations.put("additonal", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("additonal")).add(_oper);
    }

    public OperatorServiceSoapBindingSkeleton() {
        this.impl = new vn.t3h.chapter9.topdown.OperatorServiceSoapBindingImpl();
    }

    public OperatorServiceSoapBindingSkeleton(vn.t3h.chapter9.topdown.OperatorService impl) {
        this.impl = impl;
    }
    public int subtract(int x, int y) throws java.rmi.RemoteException
    {
        int ret = impl.subtract(x, y);
        return ret;
    }

    public int additonal(int x, int y) throws java.rmi.RemoteException
    {
        int ret = impl.additonal(x, y);
        return ret;
    }

}
