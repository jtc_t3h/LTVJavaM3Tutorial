/**
 * OperatorServiceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package vn.t3h.chapter9.topdown;

public class OperatorServiceServiceLocator extends org.apache.axis.client.Service implements vn.t3h.chapter9.topdown.OperatorServiceService {

    public OperatorServiceServiceLocator() {
    }


    public OperatorServiceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public OperatorServiceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for OperatorService
    private java.lang.String OperatorService_address = "http://localhost:8080/LTVJavaM3Tutorial/services/OperatorService";

    public java.lang.String getOperatorServiceAddress() {
        return OperatorService_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String OperatorServiceWSDDServiceName = "OperatorService";

    public java.lang.String getOperatorServiceWSDDServiceName() {
        return OperatorServiceWSDDServiceName;
    }

    public void setOperatorServiceWSDDServiceName(java.lang.String name) {
        OperatorServiceWSDDServiceName = name;
    }

    public vn.t3h.chapter9.topdown.OperatorService getOperatorService() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(OperatorService_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getOperatorService(endpoint);
    }

    public vn.t3h.chapter9.topdown.OperatorService getOperatorService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            vn.t3h.chapter9.topdown.OperatorServiceSoapBindingStub _stub = new vn.t3h.chapter9.topdown.OperatorServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getOperatorServiceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setOperatorServiceEndpointAddress(java.lang.String address) {
        OperatorService_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (vn.t3h.chapter9.topdown.OperatorService.class.isAssignableFrom(serviceEndpointInterface)) {
                vn.t3h.chapter9.topdown.OperatorServiceSoapBindingStub _stub = new vn.t3h.chapter9.topdown.OperatorServiceSoapBindingStub(new java.net.URL(OperatorService_address), this);
                _stub.setPortName(getOperatorServiceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("OperatorService".equals(inputPortName)) {
            return getOperatorService();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://chapter9.t3h.vn", "OperatorServiceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://chapter9.t3h.vn", "OperatorService"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("OperatorService".equals(portName)) {
            setOperatorServiceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
