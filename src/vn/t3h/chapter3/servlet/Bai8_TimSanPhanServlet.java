package vn.t3h.chapter3.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.t3h.chapter3.model.QLCSDL;
import vn.t3h.chapter3.model.SanPham;

/**
 * Servlet implementation class Bai7_TimSanPhanServlet
 */
@WebServlet("/chapter3/bai8/timsanpham.htm")
public class Bai8_TimSanPhanServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Bai8_TimSanPhanServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String tensp = request.getParameter("tensp");
		try {
			String sql = "Select * from sanpham where tensanpham like '%" + tensp + "%'";
			QLCSDL qlcsdl = new QLCSDL();
			List<SanPham> lstSanPham = qlcsdl.dsSanPham(sql);
			 
			request.setAttribute("lstSanPham", lstSanPham);
			getServletContext().getRequestDispatcher("/chapter3/bai8/chitietsanpham.jsp").forward(request, response);
		} catch (SQLException | ClassNotFoundException ex) {
			Logger.getLogger(Bai8_TimSanPhanServlet.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
