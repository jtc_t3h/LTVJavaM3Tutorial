package vn.t3h.chapter3.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sun.reflect.ReflectionFactory.GetReflectionFactoryAction;
import vn.t3h.chapter3.utils.ConfigUtils;

/**
 * Servlet implementation class Bai6_LoginServlet
 */
@WebServlet("/chapter3/bai6/login")
public class Bai6_LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Bai6_LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		if (ConfigUtils._USERNAME.equalsIgnoreCase(username) && ConfigUtils._PASSWORD.equalsIgnoreCase(password)){
			HttpSession session = request.getSession(true);
			session.setAttribute("username", username);
			
			response.sendRedirect("home.jsp");
		} else {
			request.setAttribute("msgError", "Tài khoản hoặc mật khẩu không đúng.");
			request.getRequestDispatcher("/chapter3/bai6/login.jsp").forward(request, response);
		}
	}

}
