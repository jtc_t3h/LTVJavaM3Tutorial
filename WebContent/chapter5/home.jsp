<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Static include demo</title>
</head>
<body>
  <%@ include file="./common/header.jsp" %><hr>
  <%@ include file="./common/menu.jsp" %><hr>
  <h2>Page content.</h2><hr>
  <%@ include file="./common/footer.jsp" %>
</body>
</html>