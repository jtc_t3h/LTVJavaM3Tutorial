<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<html>
<%@ include file="common/header.jsp" %>
<body>
<div id="header"></div>
<%@ include file="common/navigator.jsp" %>
<div id="center">
  <div id="center-left">
    <h2>Hệ thống quản lý bán vé xe</h2>
  </div>
  <%@ include file="common/right.jsp" %>
</div>
<%@include file="common/footer.jsp" %>
</body>
</html>