package vn.t3h.chapter9.proxy;

public class OperatorServiceProxy implements vn.t3h.chapter9.proxy.OperatorService {
  private String _endpoint = null;
  private vn.t3h.chapter9.proxy.OperatorService operatorService = null;
  
  public OperatorServiceProxy() {
    _initOperatorServiceProxy();
  }
  
  public OperatorServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initOperatorServiceProxy();
  }
  
  private void _initOperatorServiceProxy() {
    try {
      operatorService = (new vn.t3h.chapter9.proxy.OperatorServiceServiceLocator()).getOperatorService();
      if (operatorService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)operatorService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)operatorService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (operatorService != null)
      ((javax.xml.rpc.Stub)operatorService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public vn.t3h.chapter9.proxy.OperatorService getOperatorService() {
    if (operatorService == null)
      _initOperatorServiceProxy();
    return operatorService;
  }
  
  public int subtract(int x, int y) throws java.rmi.RemoteException{
    if (operatorService == null)
      _initOperatorServiceProxy();
    return operatorService.subtract(x, y);
  }
  
  public int additonal(int x, int y) throws java.rmi.RemoteException{
    if (operatorService == null)
      _initOperatorServiceProxy();
    return operatorService.additonal(x, y);
  }
  
  
}