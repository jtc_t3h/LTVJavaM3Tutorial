
package vn.t3h.chapter9.stub;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="additonalReturn" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "additonalReturn"
})
@XmlRootElement(name = "additonalResponse")
public class AdditonalResponse {

    protected int additonalReturn;

    /**
     * Gets the value of the additonalReturn property.
     * 
     */
    public int getAdditonalReturn() {
        return additonalReturn;
    }

    /**
     * Sets the value of the additonalReturn property.
     * 
     */
    public void setAdditonalReturn(int value) {
        this.additonalReturn = value;
    }

}
