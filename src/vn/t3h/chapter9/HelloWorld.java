package vn.t3h.chapter9;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface HelloWorld {

	@WebMethod
	public String sayHello(String name);
}
