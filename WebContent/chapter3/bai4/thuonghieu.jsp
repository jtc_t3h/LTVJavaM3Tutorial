<%@page import="vn.t3h.chapter3.model.ThuongHieu"%>
<%@page import="java.util.List"%>
<%@page import="vn.t3h.chapter3.model.QLCSDL"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="brands-area">
  <div class="zigzag-bottom"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="brand-wrapper">
          <div class="brand-list">
            <%
            QLCSDL qlcsdl = new QLCSDL();
            List<ThuongHieu> lstThuongHieu = qlcsdl.dsThuongHieu();
            for (ThuongHieu th: lstThuongHieu){
            %>
            <img src="img/<%=th.getHinhanh() %>" alt="<%=th.getTenthuonghieu() %>">
            <%
            }
            %>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>