<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="taglibs.jsp" %>
<div id="navigator">
  <div class="nav-left">
    <a href="<c:url value='home.jsp'/>" title="<fmt:message key="label.nav.home"/>"><fmt:message key="label.nav.home"/> </a> |
    <a href="#" title="<fmt:message key="label.nav.about"/>"><fmt:message key="label.nav.about"/> </a> | 
    <a href="#" title="<fmt:message key="label.nav.contact"/>"><fmt:message key="label.nav.contact"/> </a>
  </div>
  <div class="nav-right">
    <c:if test="${username != null }">
      <span><fmt:message key="label.nav.welcome"/> <b>${username }</b></span> &nbsp;&nbsp; | <a href="<c:url value='logout.html'/>" title="<fmt:message key="label.nav.logout"/>"><fmt:message key="label.nav.logout"/></a>
    </c:if>
  </div>
</div>
