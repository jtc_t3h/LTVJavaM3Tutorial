package vn.t3h.chapter9;

import javax.jws.WebService;

@WebService(endpointInterface="vn.t3h.chapter9.HelloWorld")
public class HelloWorldImpl implements HelloWorld {

	@Override
	public String sayHello(String name) {
		return "hello " + name;
	}

}
