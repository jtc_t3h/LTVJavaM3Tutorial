<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="vn.t3h.chapter3.model.SanPham"%>
<%@page import="java.util.List"%>
<%@page import="vn.t3h.chapter3.model.QLCSDL"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="maincontent-area">
  <div class="zigzag-bottom"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="latest-product">
          <h2 class="section-title">New Products</h2>
          <div class="product-carousel">
          <%
          QLCSDL qlcsdl = new QLCSDL();
          String sql = "Select * from sanpham order by ngaytao DESC limit 0,10";
          List<SanPham> lstSanPham = qlcsdl.dsSanPham(sql);
          NumberFormat formatter = new  DecimalFormat("#,###,###");
          for (SanPham sp: lstSanPham) {
          %>  
            <div class="single-product">
              <div class="product-f-image">
                <img src="img/<%=sp.getHinhanh() %>" alt="<%=sp.getTensanpham() %>">
                <div class="product-hover">
                  <a href="#" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i>Thêm vào giỏ hàng</a> <a href="single-product.html" class="view-details-link"><i class="fa fa-link"></i> Xem chi tiết</a>
                </div>
              </div>

              <h2>
                <a href="single-product.html"><%=sp.getTensanpham() %></a>
              </h2>

              <div class="product-carousel-price">
                <ins>$<%=formatter.format(sp.getDongiaKM()) %></ins>
                <del>$<%=formatter.format(sp.getDongia()) %></del>
              </div>
            </div>
            <%
            } 
            %>            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>