<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" />
<title>BTMS - Bus Ticket Management System</title>
</head>
<body>
  <div id="header"></div>
  <div id="navigator">
    <span>Đăng nhập</span>
  </div>
  <div id="center">
    <%
    if (request.getAttribute("msgError") != null){
    %>
    <h4 style="color: red;"><%=request.getAttribute("msgError") %></h4>
    <%
    }
    %>
    <form action="login" method="post">
      <table width="auto" border="0" cellspacing="5" cellpadding="0">
        <tr>
          <td>Tên đăng nhập</td>
          <td><input name="username" type="text" size="30" maxlength="50" /></td>
        </tr>
        <tr>
          <td>Mật khẩu</td>
          <td><input name="password" type="password" size="30" maxlength="50" /></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td><input name="submit" type="submit" value="Đăng nhập" /></td>
          <td><input name="remember" type="checkbox" checked="checked" />Ghi nhớ mật khẩu</td>
        </tr>
      </table>
    </form>
  </div>
  <div id="footer">Designed & Developed By Phạm Đức Đệ</div>
</body>
</html>