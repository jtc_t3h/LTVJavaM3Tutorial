package vn.t3h.chapter3.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class QLCSDL {

	private Connection connect() throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		String url = "jdbc:mysql://localhost:3306/phuong_perfume?useUnicode=yes&characterEncoding=UTF-8";
		String username = "root";
		String password = "";

		return DriverManager.getConnection(url, username, password);
	}

	public List<SanPham> dsSanPham(String sql) throws SQLException, ClassNotFoundException {
		List<SanPham> rs;

		try (Connection conn = this.connect()) {
			rs = new ArrayList();
			java.sql.Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				SanPham sp = new SanPham();
				sp.setId(resultSet.getInt("id"));
				sp.setTensanpham(resultSet.getString("tensanpham"));
				sp.setMota(resultSet.getString("mota"));
				sp.setHinhanh(resultSet.getString("hinhanh"));
				sp.setDongia(resultSet.getDouble("dongia"));
				sp.setDongiaKM(resultSet.getDouble("dongiaKM"));
				sp.setSoluong(resultSet.getInt("soluong"));
				sp.setNgaytao(resultSet.getDate("ngaytao"));
				sp.setHienthi(resultSet.getInt("hienthi"));
				sp.setId_loai(resultSet.getInt("id_loai"));

				sp.setId_thuongHieu(resultSet.getInt("id_thuonghieu"));
				rs.add(sp);
			}
		}
		return rs;
	}

	public List<ThuongHieu> dsThuongHieu() throws SQLException, ClassNotFoundException {
		List<ThuongHieu> rs;
		try (Connection conn = this.connect()) {
			rs = new ArrayList<>();
			java.sql.Statement statement = conn.createStatement();
			String sql = "SELECT * FROM thuonghieu";
			ResultSet resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				ThuongHieu th = new ThuongHieu();
				th.setId(resultSet.getInt("id"));
				th.setTenthuonghieu(resultSet.getString("tenthuonghieu"));
				th.setHinhanh(resultSet.getString("hinhanh"));
				rs.add(th);
			}
		}
		return rs;
	}
	
	public List<Loai> dsLoai() throws SQLException, ClassNotFoundException {
		List<Loai> rs;
		try (Connection conn = this.connect()) {
			rs = new ArrayList<>();
			java.sql.Statement statement = conn.createStatement();
			String sql = "SELECT * FROM loai";
			ResultSet resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				Loai loai = new Loai();
				loai.setId(resultSet.getInt("id"));
				loai.setTenloai(resultSet.getString("tenloai"));
				
				rs.add(loai);
			}
		}
		return rs;
	}
}
