
package vn.t3h.chapter9.stub;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the vn.t3h.chapter9 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: vn.t3h.chapter9
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Additonal }
     * 
     */
    public Additonal createAdditonal() {
        return new Additonal();
    }

    /**
     * Create an instance of {@link AdditonalResponse }
     * 
     */
    public AdditonalResponse createAdditonalResponse() {
        return new AdditonalResponse();
    }

    /**
     * Create an instance of {@link Subtract }
     * 
     */
    public Subtract createSubtract() {
        return new Subtract();
    }

    /**
     * Create an instance of {@link SubtractResponse }
     * 
     */
    public SubtractResponse createSubtractResponse() {
        return new SubtractResponse();
    }

}
