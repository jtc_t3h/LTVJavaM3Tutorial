package vn.t3h.chapter4;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class InitParamServlet
 */
public class InitParamServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ServletConfig config = null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InitParamServlet() {
        super();
    }

   
	@Override
	public void init(ServletConfig config) throws ServletException {
		System.out.println("InitParamServlet: call init method.");
		this.config = config;
		
//		System.out.println(config.getInitParameter("adminEmail"));
//		System.out.println(config.getInitParameter("adminNumber"));
//		
//		Enumeration<String> paramNames = config.getInitParameterNames();
//		while (paramNames.hasMoreElements()) {
//			String paramName = paramNames.nextElement();
//			System.out.println(config.getInitParameter(paramName));
//		}
	}



	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("InitParamServlet: call service method.");
		
//		ServletConfig config = getServletConfig();
		System.out.println(config.getInitParameter("adminEmail"));
		System.out.println(config.getInitParameter("adminNumber"));
		
		System.out.println("-----------------------------------");
		System.out.println(getInitParameter("adminEmail"));
		System.out.println(getInitParameter("adminNumber"));
		
	}

}
