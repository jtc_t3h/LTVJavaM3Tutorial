<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="vn.t3h.chapter3.model.SanPham"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="col-md-4">
  <div class="single-sidebar">
    <h2 class="sidebar-title">Tìm kiếm</h2>
    <form action="timsanpham.htm" method="get">
      <input type="text" name="tensp" placeholder="Thông tin tìm kiếm..."> <input type="submit" value="Tìm">
    </form>
  </div>
  <div class="single-sidebar">
    <h2 class="sidebar-title">Sản phẩm</h2>
    <%
    	List<SanPham> lstSanPham = (List<SanPham>) request.getAttribute("lstSanPham");
    	if (lstSanPham != null) {
    		for (SanPham sp : lstSanPham) {
    			NumberFormat formatter = new DecimalFormat("#,###,###");
    			String ddHinh = "img/" + sp.getHinhanh();
    			String dongia = formatter.format(sp.getDongia());
    			String dongiaKM = formatter.format(sp.getDongiaKM());
    %>
    <div class="thubmnail-recent">
      <img src="<%=ddHinh%>" class="recentthumb" alt="">
      <h2>
        <a href="laysanpham.htm?id_sanpham=<%=sp.getId()%>"> <%=sp.getTensanpham()%>
        </a>
      </h2>
      <div class="product-sidebar-price">
        <ins><%=dongiaKM%>vnđ</ins>
        <del><%=dongia%></del>
      </div>
    </div>
    <%
    	}
    } else {
    		if (request.getParameter("tensp") != null) {
    %>
    <div class="thubmnail-recent">
      <h2>Không tìm thấy sản phẩm</h2>
    </div>
    <%
    	}
    }
    %>
  </div>
</div>