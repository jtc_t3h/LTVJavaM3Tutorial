package vn.t3h.chapter7.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class QLCSDL {

	public static Connection connect() throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		String url = "jdbc:mysql://localhost:3306/phuong_perfume?useUnicode=yes&characterEncoding=UTF-8";
		String username = "root";
		String password = "";

		return DriverManager.getConnection(url, username, password);
	}

	
}
