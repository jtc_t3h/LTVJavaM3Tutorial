package vn.t3h.chapter3.model;

import java.io.Serializable;

public class ThuongHieu implements Serializable{

	private int id;
	private String tenthuonghieu;
	private String hinhanh;

	public int getId() {
		return id;
	}

	public String getTenthuonghieu() {
		return tenthuonghieu;
	}

	public String getHinhanh() {
		return hinhanh;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setTenthuonghieu(String tenthuonghieu) {
		this.tenthuonghieu = tenthuonghieu;
	}

	public void setHinhanh(String hinhanh) {
		this.hinhanh = hinhanh;
	}

}
