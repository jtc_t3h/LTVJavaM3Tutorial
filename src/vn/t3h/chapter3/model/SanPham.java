package vn.t3h.chapter3.model;

import java.io.Serializable;
import java.sql.Date;

public class SanPham implements Serializable {

	private int id;
	private String tensanpham;
	private String mota;
	private String hinhanh;
	private double dongia;
	private double dongiaKM;
	private int soluong;
	private Date ngaytao;
	private int hienthi;
	private int id_loai;
	private int id_thuonghieu;

	public int getId() {
		return id;
	}

	public String getTensanpham() {
		return tensanpham;
	}

	public String getMota() {
		return mota;
	}

	public String getHinhanh() {
		return hinhanh;
	}

	public double getDongia() {
		return dongia;
	}

	public double getDongiaKM() {
		return dongiaKM;
	}

	public int getSoluong() {
		return soluong;
	}

	public Date getNgaytao() {
		return ngaytao;
	}

	public int getHienthi() {
		return hienthi;
	}

	public int getId_loai() {
		return id_loai;
	}

	public int getId_thuongHieu() {
		return id_thuonghieu;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setTensanpham(String tensanpham) {
		this.tensanpham = tensanpham;
	}

	public void setMota(String mota) {
		this.mota = mota;
	}

	public void setHinhanh(String hinhanh) {
		this.hinhanh = hinhanh;
	}

	public void setDongia(double dongia) {
		this.dongia = dongia;
	}

	public void setDongiaKM(double dongiaKM) {
		this.dongiaKM = dongiaKM;
	}

	public void setSoluong(int soluong) {
		this.soluong = soluong;
	}

	public void setNgaytao(Date ngaytao) {
		this.ngaytao = ngaytao;
	}

	public void setHienthi(int hienthi) {
		this.hienthi = hienthi;
	}

	public void setId_loai(int id_loai) {
		this.id_loai = id_loai;
	}

	public void setId_thuongHieu(int id_thuonghieu) {
		this.id_thuonghieu = id_thuonghieu;
	}

}
