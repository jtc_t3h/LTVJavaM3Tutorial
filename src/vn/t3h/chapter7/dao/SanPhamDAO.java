package vn.t3h.chapter7.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import vn.t3h.chapter7.domain.SanPham;

public class SanPhamDAO {

	public List<SanPham> find(int... startIndexAndLength) {
		List<SanPham> list = new ArrayList<>();

		String sql = null;
		if (startIndexAndLength.length != 2) {
			sql = "Select * from sanpham order by ngaytao DESC";
		} else {
			sql = "Select * from sanpham order by ngaytao DESC limit " + startIndexAndLength[0] + ","
					+ startIndexAndLength[1];
		}

		try (Connection conn = QLCSDL.connect();
				Statement statement = conn.createStatement();
				ResultSet resultSet = statement.executeQuery(sql)) {
			while (resultSet.next()) {
				SanPham sp = new SanPham();
				sp.setId(resultSet.getInt("id"));
				sp.setTensanpham(resultSet.getString("tensanpham"));
				sp.setMota(resultSet.getString("mota"));
				sp.setHinhanh(resultSet.getString("hinhanh"));
				sp.setDongia(resultSet.getDouble("dongia"));
				sp.setDongiaKM(resultSet.getDouble("dongiaKM"));
				sp.setSoluong(resultSet.getInt("soluong"));
				sp.setNgaytao(resultSet.getDate("ngaytao"));
				sp.setHienthi(resultSet.getInt("hienthi"));
				sp.setId_loai(resultSet.getInt("id_loai"));

				sp.setId_thuongHieu(resultSet.getInt("id_thuonghieu"));
				list.add(sp);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return list;
	}

	public List<SanPham> findById(String id) {
		List<SanPham> list = new ArrayList<>();

		try (Connection conn = QLCSDL.connect();
				Statement statement = conn.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT * FROM sanpham WHERE id = " + id)) {
			while (resultSet.next()) {
				SanPham sp = new SanPham();
				sp.setId(resultSet.getInt("id"));
				sp.setTensanpham(resultSet.getString("tensanpham"));
				sp.setMota(resultSet.getString("mota"));
				sp.setHinhanh(resultSet.getString("hinhanh"));
				sp.setDongia(resultSet.getDouble("dongia"));
				sp.setDongiaKM(resultSet.getDouble("dongiaKM"));
				sp.setSoluong(resultSet.getInt("soluong"));
				sp.setNgaytao(resultSet.getDate("ngaytao"));
				sp.setHienthi(resultSet.getInt("hienthi"));
				sp.setId_loai(resultSet.getInt("id_loai"));

				sp.setId_thuongHieu(resultSet.getInt("id_thuonghieu"));
				list.add(sp);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return list;
	}
}
