package vn.t3h.chapter5;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTag;
import javax.servlet.jsp.tagext.Tag;

public class IfTag implements BodyTag{

	private PageContext pageContext;
	private Tag parent;
	private BodyContent bodyContent;
	
	private boolean condition;
	
	@Override
	public int doAfterBody() throws JspException {
		return SKIP_BODY;
	}

	@Override
	public int doEndTag() throws JspException {
		String content = bodyContent.getString();
		System.out.println(content);
		
		JspWriter out = pageContext.getOut();
		try {
			out.print("<h2> Hello, " + content + "</h2>");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return EVAL_PAGE;
	}

	@Override
	public int doStartTag() throws JspException {
		return EVAL_BODY_BUFFERED;
	}

	@Override
	public Tag getParent() {
		return parent;
	}

	@Override
	public void release() {
	}

	@Override
	public void setPageContext(PageContext pageContext) {
		this.pageContext = pageContext;
	}

	@Override
	public void setParent(Tag parent) {
		this.parent = parent;
	}
	
	public void setCondition(boolean condition) {
		this.condition = condition;
	}

	@Override
	public void doInitBody() throws JspException {
		
	}

	@Override
	public void setBodyContent(BodyContent bodyContent) {
		this.bodyContent = bodyContent;
		System.out.println("aaaaaaaaaaaaaaaaaaaa");
	}

}
