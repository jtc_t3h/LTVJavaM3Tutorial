package vn.t3h.chapter4.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import vn.t3h.chapter4.model.NguoiDung;
import vn.t3h.chapter4.model.QLCSDL;

/**
 * Servlet implementation class Bai1_DangNhapServlet
 */
@WebServlet("/chapter4/bai3/dangnhap.htm")
public class Bai3_DangNhapServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Bai3_DangNhapServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		try {
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			QLCSDL qlcsdl = new QLCSDL();
			NguoiDung nguoidung = (NguoiDung) qlcsdl.dangnhapNguoiDung(email, password);
			session.setAttribute("nguoidung", nguoidung);
		} catch (SQLException | ClassNotFoundException ex) {
			Logger.getLogger(Bai3_DangNhapServlet.class.getName()).log(Level.SEVERE, null, ex);
		}
		response.sendRedirect("index.jsp");
	}

}
