package vn.t3h.chapter7.domain;

import java.io.Serializable;

public class NguoiDung implements Serializable {

	private int id;
	private String email;
	private String password;
	private String hoten;
	private String diachi;
	private String dtdd;
	private int id_vaitro;

	public int getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public String getHoten() {
		return hoten;
	}

	public String getDiachi() {
		return diachi;
	}

	public String getDtdd() {
		return dtdd;
	}

	public int getId_vaitro() {
		return id_vaitro;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setHoten(String hoten) {
		this.hoten = hoten;
	}

	public void setDiachi(String diachi) {
		this.diachi = diachi;
	}

	public void setDtdd(String dtdd) {
		this.dtdd = dtdd;
	}

	public void setId_vaitro(int id_vaitro) {
		this.id_vaitro = id_vaitro;
	}

}
