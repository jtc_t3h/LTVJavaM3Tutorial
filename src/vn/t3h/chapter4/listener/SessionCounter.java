package vn.t3h.chapter4.listener;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Application Lifecycle Listener implementation class SessionCounter
 *
 */
@WebListener
public class SessionCounter implements HttpSessionListener {

	private int sessionCount = 0;
	
    /**
     * Default constructor. 
     */
    public SessionCounter() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see HttpSessionListener#sessionCreated(HttpSessionEvent)
     */
    public void sessionCreated(HttpSessionEvent se)  { 
    	synchronized(this){
			sessionCount++;
		}
		System.out.println("Session created ... Tong so session hien tai la " + sessionCount);
    }

	/**
     * @see HttpSessionListener#sessionDestroyed(HttpSessionEvent)
     */
    public void sessionDestroyed(HttpSessionEvent se)  { 
    	synchronized (this) {
			if (sessionCount > 0) {
				sessionCount--;
			}
		}
		System.out.println("Session destroyed ... Tong so session hien tai la " + sessionCount);
    }
	
}
