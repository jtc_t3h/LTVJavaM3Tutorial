<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dynamic include: jsp action include demo.</title>
</head>
<body>
  <h2>Dynamic include: jsp action include demo.</h2>
  <jsp:include page="./common/include_footer.jsp">
    <jsp:param value="2018" name="year"/>
  </jsp:include>
</body>
</html>