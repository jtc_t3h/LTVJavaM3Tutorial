package vn.t3h.chapter2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Bai2_TrungBinhServlet
 */
@WebServlet("/chapter2/bai2/trungbinh.html")
public class Bai2_TrungBinhServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Bai2_TrungBinhServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		try (PrintWriter out = response.getWriter()) {
			out.println("<!DOCTYPE html>");
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Ket qua tinh diem trung binh</title>");
			out.println("</head>");
			out.println("<body>");
			float tbhk1 = Float.parseFloat(request.getParameter("hk1"));
			float tbhk2 = Float.parseFloat(request.getParameter("hk2"));
			float tbcn = (tbhk1 + tbhk2) / 2;
			out.println("<h1>Trung bình của (" + tbhk1 + " + " + tbhk2 + ") = " + String.format("%.2f", tbcn) + "</h1>");
			out.println("</body>");
			out.println("</html>");
		}
	}
}
