<%@page import="java.util.Enumeration"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id="cart" scope="session" class="vn.t3h.chapter4.utils.ShoppingCart" />
<jsp:useBean id="nguoidung" scope="session" class="vn.t3h.chapter4.model.NguoiDung" />
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Checkout Page - Ustora Demo</title>

<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>

<!-- Bootstrap -->
<link rel="stylesheet" href="css/bootstrap.min.css">

<!-- Font Awesome -->
<link rel="stylesheet" href="css/font-awesome.min.css">

<!-- Custom CSS -->
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="css/responsive.css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
  <jsp:include page="mainmenu.jsp"></jsp:include>
  <div class="product-big-title-area">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="product-bit-title text-center">
            <h2>Shopping Cart</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="single-product-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
      <div class="row">
        <!-- tim kiem -->
        <jsp:include page="timkiem.jsp"></jsp:include>
        <div class="col-md-8">
          <div class="product-content-right">
            <%
            	if (nguoidung.getEmail() == null) {
            %>
            <div class="woocommerce">
              <div class="woocommerce-info">
                Quý khách đã có tài khoản? <a class="showlogin" data-toggle="collapse" href="#loginform-wrap" aria-expanded="false" aria-controls="login-form-wrap">Đăng nhập</a>
              </div>
              <form action="dangnhap.htm" id="login-formwrap" class="login collapse" method="post">
                <p class="form-row form-row-first">
                  <label for="username">Email <span class="required">*</span>
                  </label> <input type="text" id="username" name="username" class="input-text">
                </p>
                <p class="form-row form-row-last">
                  <label for="password">Password <span class="required">*</span>
                  </label> <input type="password" id="password" name="password" class="input-text">
                </p>
                <div class="clear"></div>
                <p class="form-row">
                  <input type="submit" value="Đăng nhập" name="login" class="button">
                </p>
                <div class="clear"></div>
              </form>
            </div>
            <!-- khach hang moi -->
            <div class="woocommerce">
              <div class="woocommerce-info">
                Quý khách mới? <a class="showlogin" data-toggle="collapse" href="#register-form-wrap" aria-expanded="false" aria-controls="login-form-wrap">Đăng ký mới</a>
              </div>
              <form action="dangky.htm" id="register-formwrap" class="login collapse" method="post">
                <p class="form-row">
                  <label for="username">Email <span class="required">*</span>
                  </label> <input type="text" id="username" name="username" class="input-text">
                </p>
                <p class="form-row">
                  <label for="password">Password <span class="required">*</span>
                  </label> <input type="password" id="password" name="password" class="input-text">
                </p>
                <p class="form-row">
                  <label for="hoten">Họ tên <span class="required">*</span>
                  </label> <input type="text" id="hoten" name="hoten" class="input-text">
                </p>
                <p class="form-row">
                  <label for="diachi">Địa chỉ <span class="required">*</span>
                  </label> <input type="text" id="diachi" name="diachi" class="input-text">
                </p>
                <p class="form-row">
                  <label for="dtdd">ĐTDĐ <span class="required">*</span>
                  </label> <input type="text" id="dtdd" name="dtdd" class="input-text">
                </p>
                <div class="clear"></div>
                <p class="form-row">
                  <input type="submit" value="Đăng ký" name="register" class="button">
                </p>
                <div class="clear"></div>
              </form>
            </div>
            <%
            	}
            %>
            <!-- khach hàng moi -->
            <div>
              <%
              	if (request.getAttribute("result") != null) {
              %>
              <h3 class="sidebartitle"><%=request.getAttribute("result")%></h3>
              <%
              	}
              %>
            </div>
            <form action="themdonhang.htm" method="post" name="checkout">
              <div id="customer_details" class="col2-set">
                <div class="col-1">
                  <div class="woocommerce-billing-fields">
                    <h3>Thông tin giao hàng</h3>
                    <p id="billing_first_name_field" class="form-row form-row-first validate-required">
                      <label class="" for="billing_first_name">Họ tên <abbr title="required" class="required">*</abbr>
                      </label> <input type="hidden" name="id_khachhang" value="<%=nguoidung.getId()%>"> <input type="text" value="<%=(nguoidung.getHoten() != null) ? nguoidung.getHoten() : '_'%>" placeholder="" id="fullname" name="hoten" class="input-text ">
                    </p>
                    <p id="billing_address_1_field" class="form-row form-row-wide address-field validate-required">
                      <label class="" for="billing_address_1">Địa chỉ giao hàng<abbr title="required" class="required">*</abbr>
                      </label> <input type="text" value="<%=(nguoidung.getDiachi() != null) ? nguoidung.getDiachi() : '_'%>" placeholder="Street address" id="billing_address_1" name="diachi" class="input-text ">
                    </p>
                    <div class="clear"></div>
                    <p id="billing_email_field" class="formrow form-row-first validate-required validate-email">
                      <label class="" for="billing_email">Email<abbr title="required" class="required">*</abbr>
                      </label> <input type="text" value="<%=(nguoidung.getEmail() != null) ? nguoidung.getEmail() : '_'%>" placeholder="" id="billing_email" name="email" class="input-text
">
                    </p>
                    <p id="billing_phone_field" class="formrow form-row-last validate-required validate-phone">
                      <label class="" for="billing_phone">ĐTDĐ <abbr title="required" class="required">*</abbr>
                      </label> <input type="text" value="<%=(nguoidung.getDtdd() != null) ? nguoidung.getDtdd() : '_'%>" placeholder="" id="billing_phone" name="dtdd" class="input-text
">
                    </p>
                    <div class="clear"></div>
                  </div>
                </div>
                <div class="col-2">
                  <div class="woocommerce-shipping-fields">
                    <h3 id="ship-to-different-address">
                      <label class="checkbox" for="ship-todifferent-address-checkbox">Giao hàng ở địa chỉ khác?</label> <input type="checkbox" value="1" name="ship_to_different_address" checked="checked" class="inputcheckbox" id="ship-to-different-address-checkbox">
                    </h3>
                    <div class="shipping_address" style="display: block;">
                      <p id="shipping_last_name_field" class="form-row form-row-last validate-required">
                        <label class="" for="shipping_fullname">Người nhận hàng <abbr title="required" class="required">*</abbr>
                        </label> <input type="text" value="" placeholder="" id="shipping_fullname" name="hotennguoinhan" class="input-text ">
                      </p>
                      <div class="clear"></div>
                      <p id="shipping_address_1_field" class="form-row form-row-wide address-field validate-required">
                        <label class="" for="shipping_address_1">Địa chỉ giao hàng<abbr title="required" class="required">*</abbr>
                        </label> <input type="text" value="" placeholder="Street address" id="shipping_address_1" name="diachigiaohang" class="input-text ">
                      </p>
                      <p id="billing_phone_field" class="form-row form-row-last validate-required validate-phone">
                        <label class="" for="shipping_phone">ĐTDĐ <abbr title="required" class="required">*</abbr>
                        </label> <input type="text" value="" placeholder="" id="shipping_phone" name="dienthoainguoinhan" class="input-text ">
                      </p>
                      <div class="clear"></div>
                    </div>
                    <p id="order_comments_field" class="form-row notes">
                      <label class="" for="order_comments">Ghi chú</label>
                      <textarea cols="5" rows="2" placeholder="Yêu cầu thêm cho đơn hàng như: ngày, giờ giao hàng..." id="order_comments" class="input-text " name="ghichu"></textarea>
                    </p>
                  </div>
                </div>
              </div>
              <h3 id="order_review_heading">Chi tiết Đơn hàng</h3>
              <div id="order_review" style="position: relative;">
                <table class="shop_table">
                  <thead>
                    <tr>
                      <th class="product-name">Sản phẩm</th>
                      <th class="product-total">Tổng</th>
                    </tr>
                  </thead>
                  <%
                  	NumberFormat formatter = new DecimalFormat("#,###,###");
                  	Enumeration e = cart.getEnumeration();
                  	String[] tmpItem;
                  	double thanhTientong = 0;
                  	// Iterate over the cart
                  	while (e.hasMoreElements()) {
                  		tmpItem = (String[]) e.nextElement();
                  		double dongia = Double.parseDouble(tmpItem[2]);
                  		int soluong = Integer.parseInt(tmpItem[3]);
                  		double thanhTien = dongia * soluong;
                  		String ten = tmpItem[1];
                  		String hinh = "img/" + tmpItem[4];
                  		thanhTientong += thanhTien;
                  %>
                  <tbody>
                    <tr class="cart_item">
                      <td class="product-name"><%=ten%><strong class="productquantity"> × <%=soluong%></strong></td>
                      <td class="product-total"><span class="amount"><%=formatter.format(thanhTien)%></span></td>
                    </tr>
                  </tbody>
                  <tfoot>
                    <%
                    	}
                    %>
                    <tr class="cart-subtotal">
                      <th>Thành tiền</th>
                      <td><span class="amount"><%=formatter.format(thanhTientong)%></span></td>
                    </tr>
                    <tr class="shipping">
                      <th>Phí giao hàng</th>
                      <td>Miễn phí</td>
                    </tr>
                    <tr class="order-total">
                      <th>Tổng</th>
                      <td><strong><span class="amount"><%=formatter.format(thanhTientong)%></span></strong></td>
                    </tr>
                  </tfoot>
                </table>
                <div id="payment">
                  <ul class="payment_methods methods">
                    <li class="payment_method_bacs"><input type="radio" data-order_button_text="" checked="checked" value="bacs" name="payment_method" class="input-radio" id="payment_method_bacs"> <label for="payment_method_bacs">Thanh toán khi nhận hàng </label></li>
                    <li class="payment_method_cheque"><input type="radio" dataorder_button_text="" value="cheque" name="payment_method" class="input-radio" id="payment_method_cheque"> <label for="payment_method_cheque">Thanh toán bằng thẻ ATM nội địa </label>
                      <div style="display: none;" class="payment_box payment_method_cheque">
                        <p>Please send your cheque to Store Name, Store Street, Store Town, Store State / County, Store Postcode.</p>
                      </div></li>
                    <li class="payment_method_paypal">
                      <input type="radio" dataorder_button_text="Proceed to PayPal" value="paypal" name="payment_method" class="input-radio" id="payment_method_paypal"> 
                      <label for="payment_method_paypal">PayPal <img alt="PayPal Acceptance Mark" src="https://www.paypalobjects.com/webstatic/mktg/Logo/AM_mc_vs_ms_ae_UK.png">
                        <a title="What is PayPal?" onclick="javascript:window.open('https://www.paypal.com/gb/webapps/mpp/paypal-popup', 'WIPaypal', 'toolbar=no, location=no, directories=no,status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060,height=700');return false;" class="about_paypal" href="https://www.paypal.com/gb/webapps/mpp/paypal-popup">What is PayPal?</a>
                      </label>
                      <div style="display: none;" class="payment_box payment_method_paypal">
                        <p>Pay via PayPal; you can pay with your credit card if you don’t have a PayPal account.</p>
                      </div>
                    </li>
                  </ul>
                  <div class="form-row place-order">
                    <input type="submit" data-value="Place order" value="Đặt hàng" id="place_order" name="dathang" class="button alt">
                  </div>
                  <div class="clear"></div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <jsp:include page="footer.jsp"></jsp:include>
  <!-- Latest jQuery form server -->
  <script src="https://code.jquery.com/jquery.min.js"></script>

  <!-- Bootstrap JS form CDN -->
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

  <!-- jQuery sticky menu -->
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.sticky.js"></script>

  <!-- jQuery easing -->
  <script src="js/jquery.easing.1.3.min.js"></script>

  <!-- Main Script -->
  <script src="js/main.js"></script>
</body>
</html>