package vn.t3h.chapter4.servlet;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import vn.t3h.chapter4.model.ChiTietDonHang;
import vn.t3h.chapter4.model.DonHang;
import vn.t3h.chapter4.model.NguoiDung;
import vn.t3h.chapter4.model.QLCSDL;
import vn.t3h.chapter4.utils.ShoppingCart;

/**
 * Servlet implementation class Bai4_ThemDonHangServlet
 */
@WebServlet("/chapter4/bai4/themdonhang.htm")
public class Bai4_ThemDonHangServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Bai4_ThemDonHangServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		try {
			NguoiDung nd = (NguoiDung) session.getAttribute("nguoidung");
			int id_khachhang = nd.getId();
			String hoten = ((request.getParameter("hotennguoinhan").equals("")) ? nd.getHoten()
					: request.getParameter("hotennguoinhan"));
			String diachi = ((request.getParameter("diachigiaohang").equals("")) ? nd.getDiachi()
					: request.getParameter("diachigiaohang"));
			String dtdd = ((request.getParameter("dienthoainguoinhan").equals("")) ? nd.getDtdd()
					: request.getParameter("dienthoainguoinhan"));
			String ghichu = request.getParameter("ghichu");
			Date date = new Date(System.currentTimeMillis());
			DonHang dh = new DonHang();
			dh.setId_khachhang(id_khachhang);
			dh.setTennguoidathang(hoten);
			dh.setDiachigiaohang(diachi);
			dh.setDienthoainguoinhan(dtdd);
			dh.setGhichu(ghichu);
			dh.setNgaydathang(date);
			QLCSDL qlcsdl = new QLCSDL();
			int id_donhang = qlcsdl.themDonHang(dh);
			System.out.println("id don hang: " + id_donhang);
			if (id_donhang != -1) {
				System.out.println("Đã thêm đơn hàng");
				ShoppingCart c = (ShoppingCart) session.getAttribute("cart");
				Enumeration e = c.getEnumeration();
				String[] tmpItem;
				// Iterate over the cart
				while (e.hasMoreElements()) {
					ChiTietDonHang ctdh = new ChiTietDonHang();
					tmpItem = (String[]) e.nextElement();
					int id_sanpham = Integer.parseInt(tmpItem[0]);
					int soluong = Integer.parseInt(tmpItem[3]);
					ctdh.setId_donhang(id_donhang);
					ctdh.setId_sanpham(id_sanpham);
					ctdh.setSoluong(soluong);
					qlcsdl.themCTDH(ctdh);
					c.removeItem(tmpItem[0]);
				}
			}
			request.setAttribute("result", "Chúng tôi đã nhận được đơn đặt hàng của quý khách.");
			getServletContext().getRequestDispatcher("/chapter4/bai4/thanhtoan.jsp").forward(request, response);
		} catch (SQLException | ClassNotFoundException | UnsupportedEncodingException ex) {
			Logger.getLogger(Bai4_ThemDonHangServlet.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

}
