<%@page import="java.util.Enumeration"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="vn.t3h.chapter3.model.SanPham"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id="cart" scope="session" class="vn.t3h.chapter4.utils.ShoppingCart" />
<%
	String id_incard = request.getParameter("id_incard");
	if (id_incard != null) {
		if (request.getParameter("delete") != null) {
			cart.removeItem(id_incard);
		}
		if (request.getParameter("update") != null) {
			int sl = Integer.parseInt(request.getParameter("amount"));
			cart.updateQuantity(id_incard, sl);
		}
	}
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-
scale=1">
<title>Giỏ hàng</title>
<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,
700,600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,70
0,300' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
<!-- Bootstrap -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="css/font-awesome.min.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="css/responsive.css">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5
elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via
file:// -->
<!--[if lt IE 9]>
<script
src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script
>
<script
src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
  <!-- main menu-->
  <jsp:include page="mainmenu.jsp"></jsp:include>
  <div class="product-big-title-area">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="product-bit-title text-center">
            <h2>Shopping Cart</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Page title area -->
  <div class="single-product-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="single-sidebar">
            <h2 class="sidebar-title">Tìm sản phẩm</h2>
            <form action="timsanpham.htm" method="get">
              <input type="text" name="tensp" placeholder="Thông tin tìm kiếm..."> <input type="submit" value="Tìm">
            </form>
          </div>
          <div class="single-sidebar">
            <h2 class="sidebar-title">Sản phẩm</h2>
            <%
            	List<SanPham> lstSanPham = (List<SanPham>) request.getAttribute("lstSanPham");
            	if (lstSanPham != null) {
            		for (SanPham sp : lstSanPham) {
            			NumberFormat formatter = new DecimalFormat("#,###,###");
            			String ddHinh = "img/" + sp.getHinhanh();
            			String dongia = formatter.format(sp.getDongia());
            			String dongiaKM = formatter.format(sp.getDongiaKM());
            %>
            <div class="thubmnail-recent">
              <img src="<%=ddHinh%>" class="recent-thumb" alt="">
              <h2>
                <a href="laysanpham.htm?id_sanpham=<%=sp.getId()%>"><%=sp.getTensanpham()%></a>
              </h2>
              <div class="product-sidebar-price">
                <ins><%=dongiaKM%>vnđ
                </ins>
                <del><%=dongia%></del>
              </div>
            </div>
            <%
            	}
            	} else {
            		if (request.getParameter("tensp") != null) {
            %>
            <div class="thubmnail-recent">
              <h2>Không tìm thấy sản phẩm</h2>
            </div>
            <%
            	}
            	}
            %>
          </div>
        </div>
        <div class="col-md-8">
          <div class="product-content-right">
            <div class="woocommerce">
              <table cellspacing="0" class="shop_table cart">
                <thead>
                  <tr>
                    <th class="product-remove">Xóa</th>
                    <th class="product-remove">Cập nhật</th>
                    <th class="productthumbnail">&nbsp;</th>
                    <th class="product-name">Sản phẩm</th>
                    <th class="product-price">Đơn giá</th>
                    <th class="product-quantity">Số lượng</th>
                    <th class="product-subtotal">Thành tiền</th>
                  </tr>
                </thead>
                <tbody>
                  <%
                  	NumberFormat formatter = new DecimalFormat("#,###,###");
                  	Enumeration e = cart.getEnumeration();
                  	String[] tmpItem;
                  	double thanhTientong = 0;
                  	// Iterate over the cart
                  	while (e.hasMoreElements()) {
                  		tmpItem = (String[]) e.nextElement();
                  		double dongia = Double.parseDouble(tmpItem[2]);
                  		int soluong = Integer.parseInt(tmpItem[3]);
                  		double thanhTien = dongia * soluong;
                  		String ten = tmpItem[1];
                  		String hinh = "img/" + tmpItem[4];
                  		thanhTientong += thanhTien;
                  %>
                  <div>
                    <!-- start form -->
                    <form action="giohang.jsp" method="get">
                      <tr class="cart_item">
                        <td class="product-remove"><input type="hidden" name="id_incard" value="<%=tmpItem[0]%>"> <input type="submit" value="X" name="delete"></td>
                        <td class="product-remove"><input type="submit" value="C" name="update"></td>
                        <td class="product-thumbnail"><a href="single-product.html"><img width="145" height="145" alt="poster_1_up" class="shop_thumbnail" src="<%=hinh%>"></a></td>
                        <td class="product-name"><a href="laysanpham.htm?id_sanpham=<%=tmpItem[0]%>"><%=ten%> </a></td>
                        <td class="product-price"><span class="amount"><%=formatter.format(dongia)%></span></td>
                        <td class="product-quantity">
                          <div class="quantity buttons_added">
                            <input type="number" size="4" class="input-text qty text" title="Qty" value="<%=soluong%>" min="0" max="10" step="1" name="amount">
                          </div>
                        </td>
                        <td class="product-subtotal"><span class="amount"><%=formatter.format(thanhTien)%></span></td>
                      </tr>
                    </form>
                  </div>
                  <%
                  	}
                  %>
                  <tr>
                    <td class="actions" colspan="6">
                      <div class="coupon">
                        <label for="coupon_code">Mã giảm giá:</label> <input type="text" placeholder="mã giảm giá" value="" id="coupon_code" class="input-text" name="coupon_code"> <input type="submit" value="Sử dụng" name="apply_coupon" class="button">
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
              <div class="cart-collaterals">
                <div class="cart_totals ">
                  <h2>Tổng giỏ hàng</h2>
                  <table cellspacing="0">
                    <tbody>
                      <tr class="cart-subtotal">
                        <th>Thành tiền</th>
                        <td><span class="amount"><%=formatter.format(thanhTientong)%></span></td>
                      </tr>
                      <tr class="shipping">
                        <th>Phí giao hàng</th>
                        <td>Miễn phí</td>
                      </tr>
                      <tr class="order-total">
                        <th>Tổng cộng</th>
                        <td><strong><span class="amount"><%=formatter.format(thanhTientong)%></span></strong></td>
                      </tr>
                      <tr class="order-total">
                        <td colspan="2"><a href="thanhtoan.jsp"><h2>THANH TOÁN</h2></a></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <jsp:include page="footer.jsp"></jsp:include>
  <!-- Latest jQuery form server -->
  <script src="https://code.jquery.com/jquery.min.js"></script>
  <!-- Bootstrap JS form CDN -->
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <!-- jQuery sticky menu -->
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.sticky.js"></script>
  <!-- jQuery easing -->
  <script src="js/jquery.easing.1.3.min.js"></script>
  <!-- Main Script -->
  <script src="js/main.js"></script>
</body>
</html>