<%@page import="vn.t3h.chapter3.model.Loai"%>
<%@page import="java.util.List"%>
<%@page import="vn.t3h.chapter3.model.QLCSDL"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="footer-top-area">
  <div class="zigzag-bottom"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-6">
        <div class="footer-about-us">
          <h2>
            Phuong<span>Perfume</span>
          </h2>
          <p>Chuyên cung cấp các loại nước hoa ngoại nhập với chất lượng đảm bảo và giá cả phải chăng</p>
          <div class="footer-social">
            <a href="#" target="_blank"><i class="fa fa-facebook"></i></a> 
            <a href="#" target="_blank"><i class="fa fa-twitter"></i></a> 
            <a href="#" target="_blank"><i class="fa fa-youtube"></i></a> 
            <a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-6">
        <div class="footer-menu">
          <h2>Loại</h2>
          <ul>
            <%
            	QLCSDL qlcsdl = new QLCSDL();
            	List<Loai> lstLoai = qlcsdl.dsLoai();
            	if (lstLoai.size() > 0) {
            		for (Loai l : lstLoai) {
            %>
            <li><a href="shop.jsp?id_loai=<%=l.getId()%>"><%=l.getTenloai()%></a></li>
            <%
            	}
            	}
            %>
          </ul>
        </div>
      </div>
      <div class="col-md-3 col-sm-6">
        <div class="footer-menu">
          <h2 id="dangnhap">Đăng nhập</h2>
          <div class="newsletter-form">
            <form action="dangnhap.htm" method="post">
              <input type="email" placeholder="Nhập email" name="email"> <input type="password" placeholder="Nhập
password" name="password"> <input type="submit" value="Đăng nhập">
            </form>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-6">
        <div class="footer-newsletter">
          <h2>Nhận tin</h2>
          <p>Vui lòng cung cấp email để nhận tin mới cập nhật thường xuyên</p>
          <div class="newsletter-form">
            <form action="#">
              <input type="email" placeholder="Nhập email"> <input type="submit" value="Nhận tin">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End footer top area -->
<div class="footer-bottom-area">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="copyright">
          <p>
            &copy; 2016 uCommerce. All Rights Reserved. <a href="http://www.freshdesignweb.com" target="_blank">freshDesignweb.com</a>
          </p>
        </div>
      </div>
      <div class="col-md-4">
        <div class="footer-card-icon">
          <i class="fa fa-cc-discover"></i> <i class="fa fa-cc-mastercard"></i> <i class="fa fa-cc-paypal"></i> <i class="fa fa-cc-visa"></i>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End footer bottom area -->